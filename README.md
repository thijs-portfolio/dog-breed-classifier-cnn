# Dog breed classifier

This projects showcases two of my CNN implementations used for object classification (in this case the breed of dogs). 

I experimented with two models, one using the VGG16 architecture which I used for feature extraction. The fully connected layers were re-trained on the dog breed image set. The resulting model is quite accurate (76%) given the limited dataset and the similar appearance of some of the dog breeds.

Because the VGG16 model is quite big and slow to run, I also experimented with my own CNN model which is much simpler. The model loads and predicts much faster, but its validation accuracy leaves much to be desired (37%). While its predictions are often incorrect, upon closer inspection the network does often does classify dogs with breeds that look quite similar in appearance to the target (though incorrect nevertheless).
In retrospect, the model should have been deeper and trained longer. Also, the image dataset of dog breeds might be insufficient to properly train a CNN from scratch.


## Usage

The model can be run via the command line. For classification of single images run the following:

    $python dog_classifier.py -f [Path to the image file]

To best see the model performance, I recommend classifying multiple images stored in a folder (passed with the -d flag) at once:

    $python dog_classifier.py -d "images"

The repository comes with some examples in the "images" folder.

By default the accurate model is loaded (transfer learned VGG16). The simpler model can be loaded using:

    $python dog_classifier.py -m "simple" -d "images"


To train either of the models the "-t" flag can be used, followed by several training hyperparameters and a model output directory. See --help for more information.


## Udacity

This project premise and dataset was provided by [Udacity](https://www.udacity.com/course/deep-learning-nanodegree--nd101) as part of their 4 month deep learning nanodegree course. This code was my graded project submission. I since rewrote everything in order to create a stand-alone command line app, and to upload it to gitlab.

.