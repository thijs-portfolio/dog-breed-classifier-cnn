"""The main program"""

import os
import argparse

import torch
import numpy as np
from matplotlib import pyplot as plt

import utils
import models
import training

parser = argparse.ArgumentParser()
parser.add_argument("-m", 
                    "--model", 
                    choices=["accurate", "simple"],
                    default=["accurate"],
                    nargs=1,
                    help="Choose the model to classify the dog breed. The default "
                         "option 'accurate' loads a transfer learned vgg16 network "
                         "which is the most accurate, but also the slowest. The "
                         "'simple' model is smaller but much less accurate.")

parser.add_argument("-f", 
                    "--file",
                    help="The path to the image file to classify.")

parser.add_argument("-d", 
                    "--image_directory",
                    help="A directory containing a collection of images to classify.")

parser.add_argument("-i", 
                    "--no_image",
                    action="store_true",
                    help="Display the image with the assigned breed label.")

parser.add_argument("-t",
                    "--train",
                    action="store_true",
                    help="Train the specified model. When this flag is set, the "
                         "following training flags are required: [--n_epochs, "
                         "--training_set, --train_output].")

parser.add_argument("-ts",
                    "--training_set",
                    help="The directory where the training set is stored. Inside this "
                         "folder there should be a 'train', 'valid' and 'test' folder. "
                         "The classification categories are subfolders within each of "
                         "these containing the images corresponding to these categories.")

parser.add_argument("-e",
                    "--n_epochs",
                    type=int,
                    const=10,
                    nargs="?",
                    help="The number of epochs to train the model")

parser.add_argument("-b",
                    "--batch_size",
                    type=int,
                    const=20,
                    nargs="?",
                    help="The batch_size used in training")

parser.add_argument("-l",
                    "--learning_rate",
                    type=float,
                    const=0.01,
                    nargs="?",
                    help="The learning rate to be used in training")

parser.add_argument("-o",
                    "--train_output",
                    help="The file path where the trained model should be saved")


                    

def show_img(fp, title=""):
    im = utils.image_preprocess(fp).numpy().squeeze()
    im = np.moveaxis(im, 0, -1)
    plt.imshow(im)
    plt.title(title)
    plt.show()


def classify(img_path, show_images=True):
    output = utils.breed_predictor(model, img_path)
    caption = "This looks like a a {}.".format(output[0])
    print(caption)
    if show_images:
        show_img(img_path, title=caption)


if __name__ == "__main__":
    args = parser.parse_args()

    if not args.train:

        ############################################################
        print("Loading the model. This may take a few seconds...")

        if args.model[0] == "accurate":
            model = models.load_model_transfer("model_transfer.pt")
        else:
            model = models.load_model_scratch("model_scratch.pt")

        # classify a single image
        if args.file is not None:
            classify(args.file, not args.no_image)

        # classify a folder of images
        if args.image_directory is not None:
            files = [imfn for imfn in next(os.walk(args.image_directory))[2] if imfn[-4:] in [".jpg", ".png"]]
            for file in files:
                classify(os.path.join(args.image_directory, file), not args.no_image)

    else:
        ############################################################
        req_args = [args.n_epochs, 
                    args.training_set, 
                    args.train_output,
                    ]

        if None in req_args:
            print("Could not train the model because not all of the required "
                  "training parameters are defined. Please see --help for more "
                  "info.")
            quit()

        if args.model[0] == "accurate":
            model = models.model_transfer_untrained(pretrained_features=True)
        else:
            model = models.model_scratch_untrained()

        data_loaders = training.dataloaders(args.training_set, args.batch_size)
        training.train(args.n_epochs, 
                       data_loaders, 
                       model, 
                       training.optimizer(model, args.learning_rate), 
                       training.criterion(), 
                       torch.cuda.is_available(), 
                       save_path=args.train_output)


